# Acquia CMS Toolbar Gin

Improves the look of the secondary toolbar when using Acquia CMS with the Gin Admin Theme.

If your site is also using Acquia Site Studio, you should also look at Acquia
[Site Studio Gin](https://www.drupal.org/project/sitestudio_gin).
For additional control over the toolbar items, including which
should be shown and how they're labelled, consider the
[Toolbar Manager](https://www.drupal.org/project/toolbar_manager) module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/acquia_cms_toolbar_gin).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/acquia_cms_toolbar_gin).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Acquia CMS Toolbar module and the Gin admin theme to be
installed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
- Pavlos Daniel - [pavlosdan](https://www.drupal.org/u/pavlosdan)